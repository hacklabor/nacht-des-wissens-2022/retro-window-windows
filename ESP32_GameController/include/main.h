#include <Arduino.h>
#include <setup.h>
enum _gameStatus{
    GS_idle,
    GS_init,
    GS_play,
    GS_demo,
    
};

#ifndef Matrix_Display
#include <Wire.h> // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h"
#include "oled.h"
SSD1306Wire display(0x3c, 5, 4);
oled disp;
#endif

#include <LedMatrix.h>
LedMatrix pixelMatrix(10, 21, 255);

#include <TimeTrigger.h>
TimeTrigger GameLoop(GameLoopTime);

#include "game.h"
int posController =0;

uint16_t Points = 0;

#include <speedCounter.h>
speedCounter SpeedPaddle(max_Speed);
speedCounter SpeedBall(max_Speed);


#include "paddle.h"
paddle myPad(PaddleSize,0);


#include <ball.h>
ball myBall1(1,1,19,1,-1,StartBallSpeed,max_Speed);




#include "BluetoothSerial.h"
String MACadd = "AA:BB:CC:11:22:33";
uint8_t address[6] = {0xAA, 0xBB, 0xCC, 0x11, 0x22, 0x33};
String name = "wippe"; //                  <------- set this to be the name of the other ESP32!!!!!!!!!
const char *pin = "1234"; //<- standard pin would be provided by default
bool connected;

#ifdef Bluetooth
BluetoothSerial SerialBT;
#endif


#include <esp_now.h>
#include <WiFi.h>
                        

typedef struct struct_message {
    char a[32];
    int b;
    float c;
    bool d;
} struct_message;

// Create a struct_message called myData
struct_message myData;