#pragma once
#include <Arduino.h>
#include <LedMatrix.h>

struct  _paddleData
{
  signed int x = 0;
  uint8_t size = 3;
  uint8_t startSize = 3;
};


class paddle
{

    
public:
  
    _paddleData PaddleData;

    

    paddle(uint8_t _siz,signed int _x);
    void moveLeft(LedMatrix &matrix,int16_t val);
    void moveRight(LedMatrix &matrix,int16_t val);
    void Show(LedMatrix &matrix);

};