#pragma once
#include <Arduino.h>
#include <LedMatrix.h>


enum _bounce
{
    B_Nothing,
    B_brick,
    B_leave,
    B_wall,
    B_paddel

};

struct _ballData
{
    signed int x = 0;
    signed int y = 0;
    uint8_t size = 1;
    int8_t xDir = 1;
    int8_t yDir = 1;
    bool activ = false;
    uint8_t speed = 5;
    uint8_t startSpeed = 5;
    uint8_t maxSpeed = 10;

};

class ball
{

public:
    _ballData ballData;

    ball(uint8_t _siz, signed int _x, signed int _y, int8_t _xDir, int8_t _yDir, uint8_t _speed, uint8_t _maxSpeed);
    
    uint8_t move(LedMatrix &matrix);
    void Show(LedMatrix &matrix);
    void SetBallSpeed(uint8_t _speed);
};