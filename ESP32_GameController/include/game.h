

#include <Arduino.h>
#include <paddle.h>
#include <LedMatrix.h>
#include <TimeTrigger.h>
#include <speedCounter.h>
#include <ball.h>




int8_t MaxSpeedBall = 10;
int8_t MaxSpeedPaddle = 10;
int8_t BallSpeed=5;



void newGame(paddle &pad, ball &bal, speedCounter &paddleSpeed, speedCounter &ballCNT, uint8_t maxSpeed,uint8_t bSpeed, uint8_t width, uint8_t hight);
bool movePaddle(LedMatrix &matrix, signed int controllerData, paddle &pad, speedCounter &paddleSpeed);
uint8_t moveBall(LedMatrix &matrix, ball &bal, speedCounter &ballSpeed);
void RunGame(LedMatrix &matrix, signed int controllerData, paddle &pad, ball &bal, speedCounter &paddleSpeed, speedCounter &ballSpeed,uint8_t maxspeed,uint8_t Helligkeit);
