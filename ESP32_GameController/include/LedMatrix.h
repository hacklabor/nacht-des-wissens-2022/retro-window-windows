#pragma once
#include <Arduino.h>



enum _color
    {
        LEDred = 0b11100000,
        LEDgreen = 0b00011100,
        LEDblue = 0b00000011,
        LED_lightBlue = 0b00000001,
        LEDwhite = 0b11111111,
        LEDblack = 0b00000000,
        LEDyellow = 0b11111100,
        LEDmagenta = 0b11100011,
        LEDcyan = 0b00011111,
        
    };

enum _objekt{
    OJ_sad,
    OJ_fun,
    OJ_lvl,
    OJ_arrowL,
    OJ_arrowR,
};



struct _matrix
{
    uint16_t width = 255;
    uint16_t high = 255;
    uint8_t brightness=20;
    uint8_t pixel[32][32];
    uint8_t paddleColor = LEDyellow;
    uint8_t ballColor = LEDwhite;
};


    enum _command{
        ImageData=1,
        ImageShow=2,
        SendBrightness=3,

    };

class LedMatrix
{
private:

uint8_t Zahlen1[10][15] = {{1, 1, 1,
                           1, 0, 1,
                           1, 0, 1,
                           1, 0, 1,
                           1, 1, 1},
                          {0, 1, 0,
                           0, 1, 0,
                           0, 1, 0,
                           0, 1, 0,
                           0, 1, 0},
                          {1, 1, 1,
                           0, 0, 1,
                           1, 1, 1,
                           1, 0, 0,
                           1, 1, 1},
                          {1, 1, 1,
                           0, 0, 1,
                           1, 1, 1,
                           0, 0, 1,
                           1, 1, 1},
                          {1, 0, 1,
                           1, 0, 1,
                           1, 1, 1,
                           0, 0, 1,
                           0, 0, 1},
                          {1, 1, 1,
                           1, 0, 0,
                           1, 1, 1,
                           0, 0, 1,
                           1, 1, 1},
                          {1, 1, 1,
                           1, 0, 0,
                           1, 1, 1,
                           1, 0, 1,
                           1, 1, 1},
                          {1, 1, 1,
                           0, 0, 1,
                           0, 0, 1,
                           0, 0, 1,
                           0, 0, 1},
                          {1, 1, 1,
                           1, 0, 1,
                           1, 1, 1,
                           1, 0, 1,
                           1, 1, 1},
                          {1, 1, 1,
                           1, 0, 1,
                           1, 1, 1,
                           0, 0, 1,
                           1, 1, 1}};

uint8_t level[8*5] = {

1,0,1,0,0,1,0,1,
1,0,1,0,0,1,0,1,
1,0,1,0,0,1,0,1,
1,0,0,1,1,0,0,1,
1,0,0,1,1,0,0,1

};

uint8_t sadSmiely[8*7]={

0,1,1,0,0,1,1,0,
0,1,1,0,0,1,1,0,
0,0,0,0,0,0,0,0,
0,0,0,1,1,0,0,0,
0,0,0,0,0,0,0,0,
0,1,1,1,1,1,1,0,
1,0,0,0,0,0,0,1

};
uint8_t funSmiely[8*7]={

0,1,1,0,0,1,1,0,
0,1,1,0,0,1,1,0,
0,0,0,0,0,0,0,0,
0,0,0,1,1,0,0,0,
0,0,0,0,0,0,0,0,
1,0,0,0,0,0,0,1,
0,1,1,1,1,1,1,0

};

uint8_t arrowLeft[4*7]={

0,0,0,1,
0,0,1,0,
0,1,0,0,
1,0,0,0,
0,1,0,0,
0,0,1,0,
0,0,0,1


};
uint8_t arrowRight[4*7]={

1,0,0,0,
0,1,0,0,
0,0,1,0,
0,0,0,1,
0,0,1,0,
0,1,0,0,
1,0,0,0


};





  void TxRs485(uint8_t command);
  void setFont( int16_t x, int16_t y, uint8_t number[], uint8_t color,uint8_t width, uint8_t hight);
public:
    
    
    _matrix Matrix;

    LedMatrix(uint16_t _width, uint16_t _high, uint8_t _brightness);
    void setPixel(uint8_t x, uint8_t y, uint8_t color);
  
    void FillScreen(uint8_t color);
    void SetBrightness(uint8_t _brightness);
    void Send();
    void Show();
    void writeNumber(int16_t x, int16_t y,uint8_t zahl,uint8_t color);
    void writeObjekt(int16_t x, int16_t y,uint8_t Objekt,uint8_t color);

};