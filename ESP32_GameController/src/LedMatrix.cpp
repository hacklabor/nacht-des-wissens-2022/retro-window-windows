
#include <Arduino.h>
#include <LedMatrix.h>

LedMatrix::LedMatrix(uint16_t _width, uint16_t _high, uint8_t _brightness)
{

    Matrix.brightness = _brightness;
    if (_width < 32)
    {
        Matrix.width = _width;
    }
    else
    {
        Matrix.width = 256;
    }
    if (_high < 32)
    {
        Matrix.high = _high;
    }
    else
    {
        Matrix.high = 256;
    }
    for (int row = 0; row < 32; row++)
    {
        for (int col = 0; col < 32; col++)
        {
            Matrix.pixel[col][row] = LEDblack;
        }
    }
}

void LedMatrix::setPixel(uint8_t x, uint8_t y, uint8_t color)
{
    Matrix.pixel[x][y] = color;
}
void LedMatrix::TxRs485(uint8_t command)
{
    const char *Start = "Start";
    uint8_t chksum = 0;

    for (int i = 0; i < 5; i++)
    {
        Serial2.write(Start[i]);
        chksum += Start[i];
    }
    Serial2.write(command);
    chksum += command;

    if (command == ImageData)

    {

        Serial2.write(Matrix.width);
        chksum += Matrix.width;
        Serial2.write(Matrix.high);
        chksum += Matrix.high;
        for (int row = 0; row < Matrix.high; row++)
        {
            for (int col = 0; col < Matrix.width; col++)
            {
                Serial2.write(Matrix.pixel[col][row]);
                chksum += Matrix.pixel[col][row];
            }
        }
    }
    else if (command == ImageShow)
    {
    }
    else if (command == SendBrightness)
    {
        Serial2.write(Matrix.brightness);
        chksum += Matrix.brightness;
    }
    Serial2.write(chksum);
}

void LedMatrix::FillScreen(uint8_t color)
{
    for (int row = 0; row < Matrix.high; row++)
    {
        for (int col = 0; col < Matrix.width; col++)
        {
            Matrix.pixel[col][row] = color;
        }
    }
}

void LedMatrix::SetBrightness(uint8_t _brightness)
{
    Matrix.brightness = _brightness;
    TxRs485(SendBrightness);
}
void LedMatrix::Send()
{
    TxRs485(ImageData);
}
void LedMatrix::Show()
{
    TxRs485(ImageShow);
}

void LedMatrix::writeNumber(int16_t x, int16_t y, uint8_t zahl, uint8_t color)
{

    if (zahl > 199)
    {
        return;
    }
    uint8_t hunderter = zahl / 100;
    uint8_t zehner = (zahl % 100) / 10;
    uint8_t einer = (zahl % 100) % 10;

    if (zahl > 99)
    {
x++;
        for (int i = 0; i < 5; i++)
        {
            Matrix.pixel[x][y + i] = color;
        }
    }
    
    
        setFont(x+2,y,Zahlen1[zehner],color,3,5);
        setFont(x+2+4,y,Zahlen1[einer],color,3,5);
   
}

void LedMatrix::setFont( int16_t x, int16_t y, uint8_t number[], uint8_t color,uint8_t width, uint8_t hight)
{

    uint8_t z = 0;



    for (int r = 0+y; r < hight+y; r++)
    {
        for (int c = 0+x; c < width+x; c++)

        {
            
            if (number[z] == 1)
            {
              if (c>=0 && c<Matrix.width){
                if (r>=0 && r<Matrix.high ){
                 Matrix.pixel[c][r] = color;
              }

              }
               
            }
            z++;
        }
    }
 
}

 void  LedMatrix:: writeObjekt(int16_t x, int16_t y,uint8_t Objekt,uint8_t color){

    if (Objekt==OJ_sad){
        setFont(x,y,sadSmiely,color,8 ,7);
    }
       if (Objekt==OJ_fun){
        setFont(x,y,funSmiely,color,8,7);
    }
       if (Objekt==OJ_lvl){
        setFont(x,y,level,color,8,5);
    }
    if (Objekt==OJ_arrowL){
        setFont(x,y,arrowLeft,color,4,7);
    }
    if (Objekt==OJ_arrowR){
        setFont(x,y,arrowRight,color,4,7);
    }

 }