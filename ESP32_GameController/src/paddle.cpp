#include <Arduino.h>
#include <paddle.h>

paddle::paddle(uint8_t _siz, signed int _x)
{
    PaddleData.x = _x;
    PaddleData.size = _siz;
    PaddleData.startSize = _siz;
}
void paddle::moveLeft(LedMatrix &matrix,int16_t val)
{ int pos = matrix.Matrix.width/2 -PaddleData.size/2+val;
if (pos<0){
    pos=0;
}

    PaddleData.x--;
    if (PaddleData.x<pos){
        PaddleData.x=pos;
    }
    paddle::Show(matrix);
}
void paddle::moveRight(LedMatrix &matrix,int16_t val)
{       int pos = matrix.Matrix.width/2 -PaddleData.size/2+val;
if (pos>matrix.Matrix.width-PaddleData.size){
    pos=matrix.Matrix.width-PaddleData.size;
}
        PaddleData.x++;
       
    if (PaddleData.x>pos){
        PaddleData.x=pos;
    }
    paddle::Show(matrix);
    
}
void paddle::Show(LedMatrix &matrix)
{
    for (signed int i = PaddleData.x; i < PaddleData.size + PaddleData.x; i++)
    {
        matrix.Matrix.pixel[i][20] = matrix.Matrix.paddleColor;
    }
}