#include <Arduino.h>
#include <LedMatrix.h>
#include <paddle.h>
#include <speedCounter.h>
#include <ball.h>
#include <block.h>
#include "coin.h"
#include <smb_bump.h>
#include <smb_fireball.h>
#include <smb_gameover.h>
#include <s_levelup.h>
#include "XT_DAC_Audio.h"
XT_Wav_Class SoundCoin(coinWav);
XT_Wav_Class SoundBump(bump_wav);
XT_Wav_Class SoundWall(fireball_wav); // create an object of type XT_Wav_Class that is used by
XT_Wav_Class SoundGameOver(gameover_wav);
XT_Wav_Class SoundLevelUp(levelup_wav); // the dac audio class (below), passing wav data as parameter.

XT_DAC_Audio_Class DacAudio(25, 0); // Create the main player class object.
                                    // Use GPIO 25, one of the 2 DAC pins and timer 0

uint32_t DemoCounter1 = 0; //

block bl[25];
uint8_t blockCollor[] = {LEDcyan, LEDred, LEDmagenta, LEDyellow, LEDgreen};
uint16_t points = 0;
signed int lastControlerVAl = 0;
uint8_t demoTimeout = 10;

enum _GameStatus
{
    GS_IDLE,
    GS_START,
    GS_NEWGAME,
    GS_RUNNING,
    GS_GAMEOVER,
    GS_LevelUP,
    GS_WAITLeft,
    GS_WAITRight,

};

uint8_t GameStatus = GS_WAITLeft;
uint8_t GameLevel = 1;
uint16_t counter = 0;
uint16_t DemoCounter = 0;
bool DemoMode = false;
void start(LedMatrix &matrix, ball &bal, paddle &pad)
{
    counter++;
    uint8_t col = LEDmagenta;
    int8_t row = matrix.Matrix.high - counter;
    if (row < 0)
    {
        counter = 0;
        GameStatus = GS_RUNNING;
    }
    else
    {
        for (int i = 0; i < 25; i++)
        {
            bl[i].Show(matrix);
        }
        bal.Show(matrix);
        pad.Show(matrix);
        for (int y = 0; y < row; y++)
        {
            for (int x = 0; x < matrix.Matrix.width; x++)
                matrix.Matrix.pixel[x][y] = col;
        }
    }
}

void GameOver(LedMatrix &matrix, ball &bal, paddle &pad)
{
    counter++;
    if (counter == 1)
    {

        if (SoundGameOver.Playing == false)
        { // if not playing,
        DacAudio.Play(&SoundGameOver);
        }
        matrix.writeObjekt(1, 5, OJ_sad, LEDred);
        matrix.writeNumber(0, 15, points, LEDwhite);
    }
    else if (counter < 160)
    {
        matrix.writeObjekt(1, 5, OJ_sad, LEDred);
        matrix.writeNumber(0, 15, points, LEDwhite);
    }
    else
    {
        points = 0;
        GameLevel = 1;
        GameStatus = GS_WAITLeft;
        bal.ballData.speed = bal.ballData.startSpeed;
        pad.PaddleData.size = pad.PaddleData.startSize;
        counter = 0;
        DemoCounter = 0;
        DemoMode = false;
    }
}
void LevelUp(LedMatrix &matrix, ball &bal, paddle &pad)
{
    counter++;

    if (counter == 1)
    {
        if (SoundLevelUp.Playing == false)
        { // if not playing,
            DacAudio.Play(&SoundLevelUp);
        }
        matrix.writeObjekt(1, 7, OJ_fun, LEDgreen);
    }

    else if (counter < 80)
    {
        matrix.writeObjekt(1, 7, OJ_fun, LEDgreen);
    }
    else if (counter < 160)
    {
        matrix.writeObjekt(1, 5, OJ_lvl, LEDwhite);
        matrix.writeNumber(0, 15, GameLevel + 1, LEDwhite);
    }
    else
    {

        GameLevel++;
        if (GameLevel <= 4)
        {
            pad.PaddleData.size = 4;
            bal.ballData.speed++;
            bal.SetBallSpeed(bal.ballData.speed);
        }
        else if (GameLevel == 5)
        {
            pad.PaddleData.size = 2;
            bal.ballData.speed--;
        }
        else
        {
            bal.ballData.speed++;
            bal.SetBallSpeed(bal.ballData.speed);
        }
        GameStatus = GS_NEWGAME;
        counter = 0;
    }
}

void newGame(paddle &pad, ball &bal, speedCounter &paddleSpeed, speedCounter &ballCNT, uint8_t maxSpeed, uint8_t width, uint8_t hight)
{
    randomSeed(millis());
    paddleSpeed.cnt.val = 0;
    paddleSpeed.cnt.maxAkt = maxSpeed;
    paddleSpeed.cnt.max = maxSpeed;
    ballCNT.cnt.val = 0;
    ballCNT.cnt.maxAkt = maxSpeed - bal.ballData.speed;
    ballCNT.cnt.max = maxSpeed;
    bal.ballData.y = hight - 2;
    bal.ballData.x = random(1, width - 1);
    bal.ballData.yDir = -1;
    bal.ballData.xDir = 1;
    points=0;

    uint8_t aktBlock = 0;
    for (int row = 0; row < 5; row++)
    {
        for (int col = 0; col < 5; col++)
        {
            bl[aktBlock].blo.x = col * 2;
            bl[aktBlock].blo.y = row + 4;
            bl[aktBlock].blo.color = blockCollor[random(sizeof(blockCollor))];
            bl[aktBlock].blo.activ = true;
            aktBlock++;
        }
    }
}

uint8_t moveBall(LedMatrix &matrix, ball &bal, speedCounter &ballSpeed)
{

    uint8_t bounce = 255;
    for (int i = 0; i < 25; i++)
    {
        bl[i].Show(matrix);
    }
    if (ballSpeed.count())
    {

        bool collision = false;
        bool playSound = false;
        uint8_t Blockanzahl = 0;
        for (int i = 0; i < 25; i++)
        {
            Blockanzahl += bl[i].checkBlock(matrix, bal, points, collision, playSound);
        }
        if (!Blockanzahl)
        {
            GameStatus = GS_LevelUP;
        }

        bounce = bal.move(matrix);
        if (playSound && SoundCoin.Playing == false)
        { // if not playing,
            DacAudio.Play(&SoundCoin);
        }
        if (bounce == B_paddel && SoundBump.Playing == false)
        { // if not playing,
            DacAudio.Play(&SoundBump);
        }
        if (bounce == B_wall && SoundWall.Playing == false)
        { // if not playing,
            DacAudio.Play(&SoundWall);
        }
    }

    matrix.writeNumber(0, 12, points, 0b00000011);
    bal.Show(matrix);

    return bounce;
}

bool movePaddle(LedMatrix &matrix, signed int controllerData, paddle &pad, speedCounter &paddleSpeed)
{
    if (controllerData != 0)
    {

        paddleSpeed.setMaxAkt(4);

        if (paddleSpeed.count())
        {

            if (controllerData > 0)
            {
                pad.moveRight(matrix, controllerData);
            }
            if (controllerData <= 0)
            {
                pad.moveLeft(matrix, controllerData);
            }
        }

        return true;
    }
    return false;
}

void RunGame(LedMatrix &matrix, signed int controllerData, paddle &pad, ball &bal, speedCounter &paddleSpeed, speedCounter &ballSpeed, uint8_t maxspeed, uint8_t Helligkeit)
{

    DacAudio.FillBuffer(); // Fill the sound buffer with data
                           // play it, this will cause it to repeat and repeat...

    if (DemoMode)

    {
        Serial.println("DemoMode");
        Serial.println(controllerData);
        Serial.println(lastControlerVAl);

        if (abs(controllerData - lastControlerVAl) > 1)
        {
            DemoMode = false;
            GameStatus = GS_WAITLeft;
            DemoCounter = 0;
            counter = 0;
        }
        lastControlerVAl = controllerData;
    }
    if (GameStatus == GS_GAMEOVER)
    {
        GameOver(matrix, bal, pad);
    }

    if (GameStatus == GS_LevelUP)
    {
        LevelUp(matrix, bal, pad);
    }

    if (GameStatus == GS_WAITLeft)
    {
        pad.PaddleData.size = 4;
        pad.PaddleData.x = 0;
        if (controllerData < -3)
        {
            if (lastControlerVAl < -3)
            {
                GameStatus = GS_WAITRight;
                counter = 0;
                DemoCounter = 0;
            }
        }
        else
        {
            matrix.writeObjekt(14 - counter, 7, OJ_arrowL, LEDgreen);
        }
        lastControlerVAl = controllerData;
        counter++;
        DemoCounter++;
        if (counter >= 18)
        {
            counter = 0;
        }
        if (DemoCounter > 20 * demoTimeout)
        {
            DemoMode = true;

            pad.PaddleData.size = 10;
            pad.PaddleData.x = 0;
            GameStatus = GS_NEWGAME;
        }
    }

    if (GameStatus == GS_WAITRight)
    {
        Serial.println("WAITRight");
        Serial.println(controllerData);
        if (controllerData > 3)
        {
            if (lastControlerVAl > 3)
            {
                GameStatus = GS_NEWGAME;
                counter = 0;
                DemoCounter = 0;
            }
        }
        else
        {
            matrix.writeObjekt(-4 + counter, 7, OJ_arrowR, LEDgreen);
        }
        lastControlerVAl = controllerData;
        counter++;
        DemoCounter++;
        if (counter >= 18)
        {
            counter = 0;
        }
        if (DemoCounter > 20 * demoTimeout)
        {
            DemoMode = true;
            pad.PaddleData.size = 10;
            pad.PaddleData.x = 0;
            GameStatus = GS_NEWGAME;
        }
    }

    if (GameStatus == GS_START)
    {
        start(matrix, bal, pad);
    }

    if (GameStatus == GS_NEWGAME)
    {
        newGame(pad, bal, paddleSpeed, ballSpeed, maxspeed, matrix.Matrix.width, matrix.Matrix.high);
        matrix.SetBrightness(Helligkeit);
        GameStatus = GS_START;
        counter = 0;
    }

    if (GameStatus == GS_RUNNING)
    {
        movePaddle(matrix, controllerData, pad, paddleSpeed);
        pad.Show(matrix);

        if (moveBall(matrix, bal, ballSpeed) == B_leave)
        {
            GameStatus = GS_GAMEOVER;
            counter = 0;
        };
    }

    //
}
