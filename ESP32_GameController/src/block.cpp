#include <Arduino.h>
#include <block.h>

block::block()
{
}
uint8_t block::moveDown()
{
    blo.x + -1;
    if (blo.x >= blo.panHight - 1)
    {
        return 1;
    }
    return 0;
}
void block::Show(LedMatrix &matrix)
{
    if (blo.activ)
    {
        matrix.Matrix.pixel[blo.x][blo.y] = blo.color;
        matrix.Matrix.pixel[blo.x + 1][blo.y] = blo.color;
    }
}

uint8_t block::checkBlock(LedMatrix &matrix, ball &bal, uint16_t &points, bool &collision, bool &playSound)
{

    uint8_t BlocksVorhanden = 0;

    if (blo.activ && !collision)
    {
        int offset = -1;
        int8_t x = bal.ballData.x;
        int8_t y = bal.ballData.y;
        int8_t xDir = bal.ballData.xDir;
        int8_t yDir = bal.ballData.yDir;

        if (yDir == 1)
        {
            offset = 1;
        }
        if (blo.y == y + offset)
        {
            if (blo.x == x + xDir || blo.x + 1 == x + xDir || blo.x == x || blo.x + 1 == x)
            {
                blo.activ = false;
                bal.ballData.yDir *= -1;
                // bal.ballData.y +=bal.ballData.yDir;
                collision = true;
                playSound =true;
                if (matrix.Matrix.pixel[x + xDir][y] > 3)
                {
                    bal.ballData.xDir *= -1;
                }
                points++;
            }
        }
    }
    if (blo.activ)
    {

        BlocksVorhanden = 1;
        Show(matrix);
    }
    

    return BlocksVorhanden;
}
