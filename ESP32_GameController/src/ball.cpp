#include <Arduino.h>
#include <ball.h>

ball::ball(uint8_t _siz, signed int _x, signed int _y, int8_t _xDir, int8_t _yDir,uint8_t _speed, uint8_t _maxSpeed)
{
    ballData.x = _x;
    ballData.y = _y;
    ballData.size = _siz;
    ballData.xDir = _xDir;
    ballData.yDir = _yDir;
    ballData.speed= _speed;
    ballData.startSpeed= _speed;
    ballData.maxSpeed= _maxSpeed;
}


uint8_t ball::move(LedMatrix &matrix)
{
    uint8_t bounce=B_Nothing;
    ballData.x += ballData.xDir;
    ballData.y += ballData.yDir;
    uint8_t x = ballData.x;
    uint8_t y = ballData.y;


    if (y >= matrix.Matrix.high-1)
    {

        
        return B_leave;
    }

 

    bool bouLeft = (x <= 0);
    bool bouRight = (x >= matrix.Matrix.width - 1);
    bool bouUp = (y <= 0);

    if (x <= 0 && y<matrix.Matrix.high-2)
    {
      
        // bounce left
        ballData.xDir *= -1;
        bounce=B_wall;
    }
    if (x >= matrix.Matrix.width - 1 && y<matrix.Matrix.high-2)
    {
       
        // bounce right
        ballData.xDir *= -1;

        bounce=B_wall;
    }
    if (y <= 0)
    {
        // bounce Uper
        ballData.yDir *= -1;
        bounce=B_wall;
    }

   
    if (ballData.yDir >0)
    {
        if (y == matrix.Matrix.high - 2)

        // check paddle bounce
        {
            
            if (matrix.Matrix.pixel[x][y + 1] ==matrix.Matrix.paddleColor ){
                if (x<=0){
                     ballData.xDir=1;
                }
                if (x >= matrix.Matrix.width - 1){
                     ballData.xDir=-1;
                }
                ballData.yDir *= -1;
                bounce=B_paddel;

            }else if(matrix.Matrix.pixel[x + ballData.xDir][y + 1] ==matrix.Matrix.paddleColor )
                {

                    ballData.xDir *= -1;
                    ballData.yDir *= -1;
                    bounce=B_paddel;
                }
                else{
                    //Check OFF
                    //ballData.yDir *= -1;

                    }
                
                
           
        }
    }
    return bounce;
}

void ball::Show(LedMatrix &matrix)
{
    
        matrix.Matrix.pixel[ballData.x ][ballData.y] = matrix.Matrix.ballColor;
    
}


void ball::SetBallSpeed(uint8_t _speed){
    if (_speed>ballData.maxSpeed){
        _speed=ballData.maxSpeed;
    }
    ballData.speed=_speed;
   
}