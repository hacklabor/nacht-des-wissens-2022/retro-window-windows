#include <Arduino.h>
#include <oled.h>

oled::oled() {}

void oled::Show(LedMatrix &matrix, SSD1306Wire &di)
{

  // draw the current demo method
  di.clear();
  for (int row = 0; row < 21; row++)
  {
    for (int col = 0; col < 10; col++)
    {
      if (matrix.Matrix.pixel[col][row] > 0)
      {
        di.fillRect(121 - row * 6, col * 6 + 2, 6, 6);
      }
    }
  }

  di.display();
}