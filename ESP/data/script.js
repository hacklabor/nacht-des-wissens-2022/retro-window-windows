let ELEMENTS=["x","y","rotation","LedJePixel","host"]


function allGreen() {
  var i;
  for (i = 0; i < ELEMENTS.length; i++){
    document.getElementById(ELEMENTS[i]).style.border = "20px solid #22ca2a";
  }
  document.getElementById("SAVEPAR").style.background = "#22ca2a";
}
 
function getValue(Name) {
 
  return document.getElementById(Name).value

}
function setValue(Name,val) {
  document.getElementById(Name).value = val;
}

 function save() {
   console.log("SavePar");
   let sendtext = [];
   var i;
   for (i = 0; i < ELEMENTS.length; i++){
       sendtext.push(getValue(ELEMENTS[i]))
 }
  post(sendtext);
  allGreen();
   
}

function fkt_boarder(ev) {
  console.log("input");
  document.getElementById(ev.target.id).style.border = "20px solid #ff0202";
  document.getElementById("SAVEPAR").style.backgroundColor = "#ff0202";
  
}

function post(text) {
  var i;
  let sendtext = "";
  for (i = 0; i < text.length; i++) {
    
    sendtext += text[i]+"/";
  }
 
     
  console.log(sendtext);
  let xhr = new XMLHttpRequest();
  xhr.open("POST", "/data", true);
  xhr.setRequestHeader("Content-Type", "text/plain");
  xhr.send(sendtext);
}

function fkt_load() {
  let xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      const DATA = this.responseText.split("/");
      var i;
      for (i = 0; i < ELEMENTS.length; i++){
        if (ELEMENTS[i]!=="host"){
          setValue(ELEMENTS[i], parseFloat(DATA[i]));
        }
        else{
          setValue(ELEMENTS[i],DATA[i]);
        }
        }
       
    
      allGreen();
       }
  };
  xhttp.open("GET", "/ParM", true);
  xhttp.send();
}


window.addEventListener("load", fkt_load);
document.getElementById("x").addEventListener("input", fkt_boarder);
document.getElementById("y").addEventListener("input", fkt_boarder);
document.getElementById("rotation").addEventListener("input", fkt_boarder);
document.getElementById("LedJePixel").addEventListener("input", fkt_boarder);
