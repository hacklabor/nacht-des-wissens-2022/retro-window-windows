#pragma once
#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <calc.h>

enum _command{
    NONE,
    IMAGE,
    SHOW,
    BRIGHTNESS 
};

class RWG_com
{
private:

byte chkSum =0;
bool RxValid = false;
byte command = NONE;
bool wait_for_Data = true;
uint16 Rx_index=0;
uint16 Img_width=0;
uint16  Img_heigth=0;
bool newImage=false;
unsigned long lastRxTime=0;
unsigned long Rx_Timeout = 5;
uint16 chkSumIndex = 65000;
uint8_t br=30;


bool chkStart(byte rx, byte pruef, byte Pos);
void reset();
   

public:


    RWG_com(uint16 _uart_rx_buffer);
    void begin(unsigned long _baud);
    void chkRxBuffer(Adafruit_NeoPixel &strip,calc &Berechnung);
    


};
