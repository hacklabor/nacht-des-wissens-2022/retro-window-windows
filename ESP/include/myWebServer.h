#pragma once
#include <Arduino.h>
#include <FileSystem.h>
#include <ESP8266WebServer.h>

struct _configData
{
    uint16_t x = 1;
    uint16_t y = 1;
    uint8_t rotation = 0;
    uint8_t LedJePixel = 4;
    String host="Panel";
};

class myWebserver
{
private:
public:
    _configData configData;

    myWebserver();

    String CreateTXString();
    String GetFile(FileSystem &fs, char *FileName);
    _configData SplitPayload(String Payload);
};
