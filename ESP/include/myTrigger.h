#pragma once
#include <Arduino.h>



struct _Trigger
{
    bool start = false;
    unsigned long TimeoutStart=0;
    unsigned long Timeout=20000;
};

class myTrigger
{
private:
_Trigger Trigger;
public:
    



    myTrigger(bool _start, unsigned long _timeout_ms);
    void start();
    void SetTimeout(unsigned long _timeout_ms);
    bool status();
};
