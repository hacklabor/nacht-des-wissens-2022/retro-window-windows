#pragma once
#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

enum rotation
{
    upper_left,  // Led0 oben links
    upper_right, // Led0 oben Rechts
    lower_left,  // Led0 unten Links
    lower_right, // Led0 unten Rechts

};

struct _LedData
{
    int xu = -1;              // x Led 0 im Gesamtbild
    int yu = -1;              // y Led 0 im Gesamtbild
    int xr = -1;              // relativer x Wert berechnet aus x, xu & rotation
    int yr = -1;              // relativer y Wert berechnet aus y, yu & rotation
    uint8 rotation = 255;     // siehe enum rotation
    uint8 Leds_per_Pixel = 0; // Anzahl reale LEDs je Pixel des Moduls
};

struct _Led
{
    int Nr = -1; // Pixel Nr im Modul
    byte r = 0;  // Rot
    byte g = 0;  // Grün
    byte b = 0;  // Blau
};

class calc
{
private:
    _Led LedrNrCalc(int a, int b, uint8 color, Adafruit_NeoPixel &strip);
    _LedData LedData;

public:
    /*
           xu = x Position von LED0 (Anschluss)  des Gesamtbildes
           yu = y Position von LED0  des Gesamtbildes
    _rotation = Ausrichtung Modul Pos von LED0 (upper_left, upper_right, lower_right, lower_left)
    _Leds_per_pixel = Anzahl der Leds je realen Pixel (hier 4)
    */

    calc(uint16 xu, uint16 yu, uint8 _rotation, uint8 _Leds_per_pixel);
    // Ändern der Configuration des Modules zur Laufzeit
    void setMatrixConfig(uint16 xu, uint16 yu, uint8 _rotation, uint8 _Leds_per_pixel);

    /*
     x,y = Pixel im Gesamtbild
     color = Farbe des Pixels (rrrgggbb)
     Funktion setz die LED entsprechende LED dem Neopix Memory. Es erfolgt aber kein strip.show()
     */

    _Led setLED(uint16 x, uint16 y, uint8 color, Adafruit_NeoPixel &strip);
};
