#include <Arduino.h>
#include <RWG_com.h>
#include <calc.h>

RWG_com::RWG_com(uint16 _uart_rx_buffer)
{
    Serial.setRxBufferSize(_uart_rx_buffer);
}
void RWG_com::begin(unsigned long _baud)
{
    Serial.begin(_baud);
}

void RWG_com::reset()
{
    Rx_index = 0;
    chkSum = 0;
    command = NONE;
    chkSumIndex = 0;
    Img_width = 0;
    Img_heigth = 0;
}

bool RWG_com::chkStart(byte rx, byte pruef, byte Pos)
{
    if (Rx_index != Pos)
    {
        return 0;
    }
    if (rx == pruef)
    {
        Rx_index += 1;
        chkSum += rx;
        return 1;
    }
    else
    {
        reset();
        return 0;
    }
}

void RWG_com::chkRxBuffer(Adafruit_NeoPixel &strip, calc &Berechnung)
{

    if (millis() - lastRxTime > Rx_Timeout)
    {
        // Serial.println("Reset");
        reset();
    }
    String deb = "";
    while (Serial.available())
    {
        lastRxTime = millis();
        byte rx = Serial.read();
        //deb = String(rx) + ", " + String(Rx_index);
        //Serial.println(deb);

        if (command == IMAGE)
        {
            if (Rx_index == 6)
            {
                Img_width = rx;
                //deb = "width " + String(Img_width);
                //Serial.println(deb);
            }
            if (Rx_index == 7)
            {
                Img_heigth = rx;
                chkSumIndex = 7 + Img_width * Img_heigth + 1;
                //deb = "hight " + String(Img_heigth) + ", " + String(chkSumIndex);
                //Serial.println(deb);
            }
            if (Rx_index > 7)
            {
                uint16 pos_x = (Rx_index - 8) % Img_width;
                uint16 pos_y = (Rx_index - 8) / Img_width;
                _Led debLed;
                debLed = Berechnung.setLED(pos_x, pos_y, rx, strip);
                //String da = String(pos_x) + "," + String(pos_y) + " N: " + String(debLed.Nr) + " R: " + String(debLed.r) + " G: " + String(debLed.g) + " B: " + String(debLed.b);
                //Serial.println(da);
            }
            if (Rx_index == chkSumIndex)
            {
                if (chkSum == rx)
                {
                    newImage = true;
                    //Serial.println("imageTrue");
                }
                reset();
            }
            chkSum += rx;
            Rx_index += 1;
        }
        if (command == SHOW)
        {
           
            if (Rx_index == chkSumIndex)
            {
                if (chkSum == rx && newImage)
                {
                    //Serial.println("Show");
                    strip.show();
                    newImage=false;
                    reset();
                }
            }
            chkSum += rx;
            Rx_index += 1;
        }
        if (command == BRIGHTNESS)
        {
            if (Rx_index == 6)
            {
                br = rx * 2.55;
                if (rx < 3)
                {
                    br = 3 * 2.55;
                }
            }
            if (Rx_index == chkSumIndex)
            {
                if (chkSum == rx)
                {
                    strip.setBrightness(br);
                    strip.show();
                    reset();
                }
            }
            chkSum += rx;
            Rx_index += 1;
        }
        if (Rx_index == 5)
        {
            if (rx == IMAGE)
            {
                command = IMAGE;
                chkSum += rx;
                Rx_index += 1;
            }
            else if (rx == SHOW)
            {
                command = SHOW;
                chkSumIndex = 6;
                chkSum += rx;
                Rx_index += 1;
            }
            else if (rx == BRIGHTNESS)
            {
                command = BRIGHTNESS;
                chkSumIndex = 7;
                chkSum += rx;
                Rx_index += 1;
            }
            else
            {
                reset();
            }
        }
        chkStart(rx, 0x74, 4);
        chkStart(rx, 0x72, 3);
        chkStart(rx, 0x61, 2);
        chkStart(rx, 0x74, 1);
        chkStart(rx, 0x53, 0);
    }
}
