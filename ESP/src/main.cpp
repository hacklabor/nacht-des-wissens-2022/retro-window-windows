#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Adafruit_NeoPixel.h>
#include <secret.h>
#include <FileSystem.h>
#include <calc.h>
#include <RWG_com.h>
#include <ElegantOTA.h>
#include <myWebServer.h>
#include <myTrigger.h>

myWebserver myWeb;
uint16 counter = 0;

#define LED_PIN D4
#define LED_COUNT 5 * 7 * 4

;
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
FileSystem tape;
calc Berechnung(0, 4, lower_left, 1);
RWG_com Rs485(256);
ESP8266WebServer server(80);
String webDat = "";

bool panelLocate = false;

myTrigger TriggerShowLocation(false, 10000);
myTrigger TriggerWebSupport(true, 300000);

void showLocation()
{
  for (int i = 0; i < LED_COUNT; i++)
  {
    uint32_t color = strip.Color(0, 255, 0);
    if (i < (1 * myWeb.configData.LedJePixel))
    {
      color = strip.Color(255, 0, 0);
    }

    strip.setPixelColor(i, color);
  }
  strip.show();
  delay(10);
}
void readParameter()
{
  if (tape.mount())
  {
    // Serial.println("Filesystem gemountet");
  }
  else
  {
    // Serial.println("Filesystem nicht gemountet");
  }

  String TXT = tape.ReadFile((char *)"/ParaM");
  String mes = "";

  if (TXT == "notExists")
  {
    mes = myWeb.CreateTXString();
    // Serial.println(mes);
    mes += "/";
    tape.WriteNewFile((char *)"/ParaM", (char *)mes.c_str());
    TXT = tape.ReadFile((char *)"/ParaM");
  }

  myWeb.configData = myWeb.SplitPayload(TXT);

  mes = myWeb.CreateTXString();

  Berechnung.setMatrixConfig(myWeb.configData.x, myWeb.configData.y, myWeb.configData.rotation, myWeb.configData.LedJePixel);
}

void Wifi_ElegantOTA()
{
  WiFi.mode(WIFI_STA);
  WiFi.setHostname(myWeb.configData.host.c_str());
  WiFi.begin(STASSID, STAPSK);
  char *host = &myWeb.configData.host[0];
  WiFi.setHostname(host);

  while (WiFi.waitForConnectResult() != WL_CONNECTED)
  {
    // Serial.println("Connection Failed! Rebooting...");
    delay(5000);
  }
  server.on("/", []()
            { server.send(200, "text/html", myWeb.GetFile(tape, (char *)"/index.html"));
              TriggerWebSupport.start();
              TriggerShowLocation.start(); });
  server.on("/style.css", HTTP_GET, []()
            { server.send(200, "text/css", myWeb.GetFile(tape, (char *)"/style.css")); });
  server.on("/script.js", HTTP_GET, []()
            { server.send(200, "text/javascript", myWeb.GetFile(tape, (char *)"/script.js")); });
  server.on("/ParM", HTTP_GET, []()
            {
               String message = myWeb.CreateTXString();
               //Serial.println(message);
              server.send_P(200, "text/plain", message.c_str()); });

  server.on("/data", HTTP_POST, []()
            {
              if (server.hasArg("plain") == false)
              { // Check if body received

                server.send(200, "text/plain", "Body not received");
                return;
              }

              String message = "";
              message += server.arg("plain");
              
              server.send(200, "text/plain", message);
              myWeb.configData = myWeb.SplitPayload(message); 
              tape.WriteNewFile((char *)"/ParaM", (char *)message.c_str());
              Berechnung.setMatrixConfig(myWeb.configData.x,myWeb.configData.y,myWeb.configData.rotation,myWeb.configData.LedJePixel) ; }

  );

  ElegantOTA.begin(&server); // Start ElegantOTA
  server.begin();
  // Serial.println("HTTP server started");

  // Serial.print("IP address: ");
  // Serial.println(WiFi.localIP());
}

void setup(void)
{
  Rs485.begin(115200);
  strip.begin();
  strip.setBrightness(30);
  readParameter();

  Wifi_ElegantOTA();
  showLocation();
  delay(2000);
  strip.clear();
  strip.show();
}

void loop()

{
  if (TriggerShowLocation.status())
  {
    showLocation();
  }

  if (TriggerWebSupport.status())
  {
    server.handleClient();
  }
  else
  {
    // server.handleClient();
    server.close();
  }

  _Led debLed;

  Rs485.chkRxBuffer(strip, Berechnung);

  // debLed = Berechnung.setLED(x, y,0b11100000, strip);

  // Serial.println(micros()-st);

  // strip.show();
}