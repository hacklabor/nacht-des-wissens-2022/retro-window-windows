#include <Arduino.h>
#include <myWebServer.h>

myWebserver::myWebserver(){}

String myWebserver::CreateTXString()
{
        String message = "";

        message += String(configData.x) + "/";
        message += String(configData.y) + "/";
        message += String(configData.rotation) + "/";
        message += String(configData.LedJePixel)+ "/";
        message += String(configData.host);

        return message;
}

String myWebserver::GetFile(FileSystem &fs, char *FileName)
{

        String TXT = fs.ReadFile(FileName);
        return TXT;
}

_configData myWebserver::SplitPayload(String Payload)
{
        String val1[20];
        int z = 0;
        int s = 0;
        int n = -1;
        while (true)
        {
                n = Payload.indexOf('/', s);
                if (n == -1)
                {
                        break;
                }
                if (s - 1 == Payload.lastIndexOf('/'))
                {
                        break;
                }

                String va = Payload.substring(s, n);
              
                val1[z] = va;
                z++;
                s = n + 1;
        }
        _configData data;
        s = 0;
        
                data.x = val1[s].toInt();
                s++;
                data.y = val1[s].toInt();
                s++;
                data.rotation = val1[s].toInt();
                s++;
                data.LedJePixel = val1[s].toInt();
                s++;
                data.host = val1[s];
        return data;
}
