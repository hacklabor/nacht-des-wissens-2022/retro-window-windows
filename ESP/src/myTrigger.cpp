#include <Arduino.h>
#include <myTrigger.h>

   myTrigger::myTrigger(bool _start, unsigned long _timeout_ms){
    Trigger.start=_start;
    Trigger.Timeout=_timeout_ms;
    Trigger.TimeoutStart=millis();
   }
void myTrigger::start(){
    Trigger.start=true;
    Trigger.TimeoutStart=millis();
}

void myTrigger::SetTimeout(unsigned long _timeout_ms){
 Trigger.Timeout=_timeout_ms;
}

bool myTrigger::status(){
    if (Trigger.start == false){
        return false;
    }
     if (millis()- Trigger.TimeoutStart >= Trigger.Timeout){
        Trigger.start=false;
        return false;
     
     }
     return true;
}
    