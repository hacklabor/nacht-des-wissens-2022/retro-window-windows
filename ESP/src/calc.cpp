#include <Arduino.h>
#include <calc.h>

calc::calc(uint16 xu, uint16 yu, uint8  _rotation, uint8 _Leds_per_pixel)
{

    LedData.xu=xu;
    LedData.yu=yu;
    LedData.rotation=_rotation;
    LedData.Leds_per_Pixel=_Leds_per_pixel;
    
}

void calc::setMatrixConfig (uint16 xu, uint16 yu, uint8  _rotation, uint8 _Leds_per_pixel){

    LedData.xu=xu;
    LedData.yu=yu;
    LedData.rotation=_rotation;
    LedData.Leds_per_Pixel=_Leds_per_pixel;

}

_Led  calc::setLED(uint16 x, uint16 y, uint8  color,Adafruit_NeoPixel &strip){
   
    _Led LedNr;


     if (LedData.rotation==upper_left){
         LedData.xr = x-LedData.xu;
    LedData.yr = y-LedData.yu;
        LedNr=LedrNrCalc(LedData.xr,LedData.yr,color,strip);
     }
       if (LedData.rotation==lower_right){
         LedData.xr = x-(LedData.xu-4);
        LedData.yr = y-(LedData.yu-6);
        LedNr=LedrNrCalc(LedData.xr,LedData.yr,color,strip);
     }
        if (LedData.rotation==lower_left){
            LedData.xr = x-LedData.xu;
    LedData.yr = y-LedData.yu;
        LedNr=LedrNrCalc(LedData.yr,LedData.xr,color,strip);
     }
       if (LedData.rotation==upper_right){

          LedData.xr = x-(LedData.xu-6);
        LedData.yr = y-(LedData.yu+4);
         LedNr=LedrNrCalc(LedData.yr,LedData.xr,color,strip);
     }
     return LedNr;
}



_Led  calc::LedrNrCalc (int a, int b,uint8  color,Adafruit_NeoPixel &strip){
     _Led Led;
      Led.Nr=-1;
        int mul =1;
     if (LedData.rotation==upper_right || LedData.rotation==lower_left){

        mul=-1; // Querformat y
     }
    
    bool vallid=b<7 && b>-1;
       
    if (a== 0*mul && vallid){
        Led.Nr=b;
    }	
    else if (a==1*mul && vallid)
    {
        Led.Nr=13-b;
    }
     else if (a==2*mul && vallid)
    {
        Led.Nr=14+b;
    }
       else if (a==3*mul && vallid)
    {
        Led.Nr=27-b;
    }
      else if (a==4*mul && vallid)
    {
        Led.Nr=28+b;
    }
    if ((LedData.rotation==lower_right or LedData.rotation==upper_right)&&vallid && Led.Nr>-1){
     
       Led.Nr=34-Led.Nr;
    }
    


    if (Led.Nr!=-1){
        Led.r=(color >> 5)*255/7;
        Led.g=((color & 0b00011111) >> 2)*255/7;
        Led.b=(color & 0b00000011) *(255/3);
        for (int i=0 ;i<LedData.Leds_per_Pixel;i++){
        strip.setPixelColor(Led.Nr*LedData.Leds_per_Pixel+i,strip.Color(Led.r,Led.g,Led.b));
        }
    }
   
    return Led;
 } 
