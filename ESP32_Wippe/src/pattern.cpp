#include <Arduino.h>
#include <pattern.h>

pattern::pattern(uint32_t _anzahl,uint8_t _grenze,uint8_t _maxVal)
{ /* args */
    Data.anzahl = _anzahl;
    Data.status =PS_Start;
    Data.grenze = _grenze;
    Data.maxVal= _maxVal;
}
bool pattern::leftORright(int val)
{

    if ((val > Data.grenze && Data.status ==PS_Start) || (val < Data.grenze * -1 && Data.status ==PS_LEFT_DONE))

    {
        if (abs(abs(val) - Data.oldVal) < 3)
        {
            return true;
        }
    }
    Data.oldVal = abs(val);
    return false;
}

_Data pattern::check(int val)
{

    if (Data.status ==PS_Start)
    { 

        if (leftORright(val))

        {

            Data.valLeft += val;

            Data.count++;

            if (Data.count >= Data.anzahl)
            {
                Data.valLeft /= Data.count;
                Data.count = 0;
                Data.valRight = 0;
                Data.status =PS_LEFT_DONE;
             
            }
        }
    }

    if (Data.status ==PS_LEFT_DONE)
    {
        if (leftORright(val))
        {
            Data.valRight += val;
            Data.count++;

            if (Data.count >= Data.anzahl)
            {
                Data.valRight /= Data.count;
                Data.count = 0;
                Data.status =PS_DONE;
                         
                if (abs(Data.valRight) <Data.valLeft)
                {
                    Data.maxVal = abs(Data.valRight);
                }
                else
                {
                    Data.maxVal = Data.valLeft;
                }

            
                
            }
        }
    }
    return Data;
}

void pattern::  setStatus(uint8_t _status){
Data.status=_status;
}

int pattern::scale(int val,uint8_t _minVal,uint8_t _maxVal,uint8_t _minScaledSpeed,uint8_t _maxScaledSpeed){
int sval =0;
uint rangeVal = _maxVal-_minVal;
uint rangeScaledVal = _maxScaledSpeed-_minScaledSpeed;

    if (abs(val)<=_minVal){
        return 0;
    }
    int v=abs(val)-_minVal;
    sval=(v*rangeScaledVal/rangeVal)+_minScaledSpeed;
  
  
    if (sval<_minScaledSpeed){
        sval=_minScaledSpeed;
    }
       if (sval>_maxScaledSpeed){
        sval=_maxScaledSpeed;
    }
      if (val<0){
         sval*=-1;
    }
  
    return sval;
}