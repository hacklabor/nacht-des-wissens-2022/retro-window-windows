#pragma once
#include <Arduino.h>

enum _patternStatus{
PS_Start,
PS_LEFT_DONE,

PS_DONE,

};

 struct _Data
    {
        uint8_t status=PS_Start;
        uint8_t maxVal=10;
        uint16_t count=0;
        uint16_t anzahl=0;
        int16_t valLeft=0;
        int16_t valRight=0;
        uint16_t grenze=0;
        uint16_t oldVal=0;
        
        int offset=0;

    };


class pattern
{
private:
   

    _Data Data;
    bool leftORright(int val);
    
    
public:
    pattern(uint32_t _anzahl,uint8_t _grenze,uint8_t _maxVal);
    _Data check(int val);
    void setStatus(uint8_t);
    int scale(int val,uint8_t _minVal,uint8_t _maxVal,uint8_t _minScaledSpeed,uint8_t _maxScaledSpeed);
    
};
  





